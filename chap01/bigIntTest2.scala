import java.math.BigInteger

def fanctional(x: BigInteger): BigInteger =
    if(x  == BigInteger.ZERO)
        BigInteger.ONE
    else
        x.multiply(fanctional(x.subtract(BigInteger.ONE)))
